This is a very annoying issue found only with a few people.
For some reason, the system's host name changes, not following what's set on your <kbd>/etc/hosts</kbd>, <kbd>/etc/hostname</kbd>
and if you use **OpenRC** like me <kbd>/etc/conf.d/hostname</kbd>.

The explanation for this to happen is that the Network Manager is set change your hostname based on the network you are connected to.
When this happens, and you are using X11 your system is unable to open new windows of programs, that means that you will be stuck with the windows you already have open.

Sometimes rebooting the system helps, sometimes resetting the hostname in the files mentioned earlier also helps, but what about the momments these does not help?
If this happens and just like me you're stuck with a random hostname, what you need to do is:

1. If you're on a graphical sessions and stuck to it just click <kbd>Control+Alt+F2</kbd> to leave to a terminal session.
2. Supposing that you use *NeoVim* you will have to do as follows:

    `$ sudo nvim /etc/connman/main.conf`

3. Locate the following this line <kbd>AllowHostnameUpdates</kbd> and set it to false.

    ![Neovim showing connman config file](https://gitlab.com/keven-mate/linux-issues-and-fixes/-/raw/main/source/img/hostaname_changed/Screenshot-2023-Sep-13--17-45-12.png?ref_type=heads "Has to be set to false")

    Just as a reference, it's this line: ![Config line highlighted](https://gitlab.com/keven-mate/linux-issues-and-fixes/-/raw/main/source/img/hostaname_changed/Screenshot-2023-Sep-13--17-45-54.png?ref_type=heads)

4. Save the file and reboot.

Now your hostname will no longer change, and all issues that come from it will magically vanish.
