In this repo I will post about issues I found using Linux and their possible solutions.

|Description                                    | Link                                                                                                                  |
|-----------------------------------------------|-----------------------------------------------------------------------------------------------------------------------|
|Distro                                         | [Artix Linux](https://artixlinux.org "Systemd-free Arch-based distro")                                                |
|init System                                    | [OpenRC](https://wiki.gentoo.org/wiki/OpenRC "Dependency-base init system")                                           |
|Network Manager                                | [Connman](https://wiki.archlinux.org/title/ConnMan "Alternative to NetworkManager and sort of default in Artix")      |
|Window Manager                                 | [BSPWM](https://github.com/baskerville/bspwm "BSPWM")                                                                 |
|Display Server                                 | [Xorg (X11)](https://wiki.archlinux.org/title/xorg) / [Wayland](https://wayland.freedesktop.org/)                     |
|Video Driver                                   | [modesetting](https://wiki.archlinux.org/title/kernel_mode_setting "Kernel's default driver")                         |
|Laptop                                         | HP 15s-eq1xxx (with an AMD Ryzen 5 4500U CPU with Radeon Graphics)                                                    |
|Kernel                                         | [Linux LTS 6.1](https://endoflife.date/linux "Long Term Support")                                                     |
|SHELL                                          | [ZSH](https://zsh.sourceforge.io/ "Z SHELL")                                                                          |
|PROMPT                                         | [Starship](https://starship.rs/)                                                                                      |

Most (if not all) of my posts/fixes will be made following the setup I have, I may end up posting fixes for other setups as well if I have a way of testing them.

1. [Hostname Randomly Changing "when using ConnMan"](https://gitlab.com/keven-mate/linux-issues-and-fixes/-/tree/main/source/text/hostname_changed?ref_type=heads "Fixed")


